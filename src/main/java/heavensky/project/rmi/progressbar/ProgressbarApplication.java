package heavensky.project.rmi.progressbar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProgressbarApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProgressbarApplication.class, args);
    }

}
