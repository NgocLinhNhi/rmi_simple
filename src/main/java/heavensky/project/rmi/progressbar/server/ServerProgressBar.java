package heavensky.project.rmi.progressbar.server;

import heavensky.project.rmi.progressbar.service.ProgressBarServiceImpl;

import javax.swing.*;
import java.awt.*;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

import static heavensky.project.rmi.progressbar.constant.RMIConstant.SERVER_TITLE;

public class ServerProgressBar {

    private int i;
    private JProgressBar bar;
    private static ServerProgressBar INSTANCE;

    public static ServerProgressBar getInstance() {
        if (INSTANCE == null) INSTANCE = new ServerProgressBar();
        return INSTANCE;
    }

    public void buildServer() throws RemoteException, MalformedURLException {
        buildSwingForm();
        buildService();
    }

    private void buildSwingForm() {
        JFrame f = new JFrame();
        bar = new JProgressBar(0, 60);

        f.setLayout(new FlowLayout());
        f.add(bar);

        f.setSize(500, 300);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setTitle(SERVER_TITLE);
    }

    private void buildService() throws RemoteException, MalformedURLException {
        ProgressBarServiceImpl progressBarService = new ProgressBarServiceImpl();
        progressBarService.time = new Timer(200, e -> bar.setValue(i++));
        createConfigServerRMI(progressBarService);
    }

    private void createConfigServerRMI(ProgressBarServiceImpl progressBarService)
            throws RemoteException, MalformedURLException {
        LocateRegistry.createRegistry(1987);
        Naming.rebind("rmi://localhost:1987/ServerProgressBar", progressBarService);
    }

}
