package heavensky.project.rmi.progressbar.constant;

public class RMIConstant {
    public static final String CLIENT_TITLE = "CLIENT RMI";
    public static final String SERVER_TITLE = "SERVER RMI";
    public static final String BUTTON_START = "Start";
    public static final String BUTTON_STOP = "Stop";
}
