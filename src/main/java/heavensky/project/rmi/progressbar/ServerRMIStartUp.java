package heavensky.project.rmi.progressbar;

import heavensky.project.rmi.progressbar.server.ServerProgressBar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.rmi.RemoteException;

public class ServerRMIStartUp {
    private static final Logger logger = LoggerFactory.getLogger(ServerRMIStartUp.class);

    public static void main(String[] args) {
        try {
            ServerProgressBar.getInstance().buildServer();
        } catch (RemoteException e) {
            logger.error("RemoteException {1}", e);
        } catch (MalformedURLException ex) {
            logger.error("MalformedURLException {1}", ex);
        }
    }
}
