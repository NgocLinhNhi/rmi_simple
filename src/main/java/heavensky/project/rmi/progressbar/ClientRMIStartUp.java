package heavensky.project.rmi.progressbar;

import heavensky.project.rmi.progressbar.client.ClientProgressBar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class ClientRMIStartUp {
    private static final Logger logger = LoggerFactory.getLogger(ClientRMIStartUp.class);

    public static void main(String[] args) {
        try {
            ClientProgressBar.getInstance().start();
        } catch (RemoteException | MalformedURLException | NotBoundException e) {
            logger.error("Exception " + e);
        }
    }
}
