package heavensky.project.rmi.progressbar.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IBar extends Remote {
    void run() throws RemoteException;

    void stop() throws RemoteException;

}
