package heavensky.project.rmi.progressbar.service;

import heavensky.project.rmi.progressbar.interfaces.IBar;

import javax.swing.*;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class ProgressBarServiceImpl extends UnicastRemoteObject implements IBar {

    public Timer time;

    public ProgressBarServiceImpl() throws RemoteException {
    }

    @Override
    public void run() {
        time.start();
    }

    @Override
    public void stop() {
        time.stop();
    }
}
