package heavensky.project.rmi.progressbar.client;


import heavensky.project.rmi.progressbar.interfaces.IBar;

import javax.swing.*;
import java.awt.*;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static heavensky.project.rmi.progressbar.constant.RMIConstant.*;

public class ClientProgressBar extends JFrame {

    private static ClientProgressBar INSTANCE;

    public static ClientProgressBar getInstance() {
        if (INSTANCE == null) INSTANCE = new ClientProgressBar();
        return INSTANCE;
    }

    private static IBar connection;

    public void start() throws RemoteException, NotBoundException, MalformedURLException {
        buildSwingForm();
        getConfigServerRmi();
    }

    private void buildSwingForm() {
        JButton btnStart = new JButton(BUTTON_START);
        JButton btnStop = new JButton(BUTTON_STOP);

        Container con = getContentPane();
        setLayout(new FlowLayout());

        con.add(btnStart);
        con.add(btnStop);

        btnStart.addActionListener(e -> addRunEvent());
        btnStop.addActionListener(e -> addStopEvent());
        buildSizeForm();
    }

    private void buildSizeForm() {
        setTitle(CLIENT_TITLE);
        setSize(500, 300);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void addRunEvent() {
        try {
            connection.run();
        } catch (RemoteException ex) {
            Logger.getLogger(ClientProgressBar.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void addStopEvent() {
        try {
            connection.stop();
        } catch (RemoteException ex) {
            Logger.getLogger(ClientProgressBar.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void getConfigServerRmi() throws RemoteException, NotBoundException, MalformedURLException {
        connection = (IBar) Naming.lookup("rmi://localhost:1987/ServerProgressBar");
    }

}
